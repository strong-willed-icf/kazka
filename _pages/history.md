---
layout: page
title: pages.history
permalink: /history
---

{% tf history1.md %}

{% include carousel.html page="history" id="konovaltsa" %}

> {% t history.carousel-caption.konovaltsa %}

{% tf history2.md %}

{% include carousel.html page="history" id="observatorna" %}

> {% t history.carousel-caption.observatorna %}

{% tf history3.md %}