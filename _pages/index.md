---
permalink: /
layout: default
classname: index-page
---

<!-- Home Jumbotron
================================================== -->
<section class="intro d-flex align-items-center">
    <div class="wrapintro">
        <h1>{% t site.name %}</h1>
        <h4>{% t site.description %}</h4>
    </div>
</section>

<!-- Featured
================================================== -->
<section class="featured-posts">
    <div class="section-title">
        <h2><span>{% t index.news %}</span></h2>
    </div>
    <div class="row listfeaturedtag">
    {% for post in site.posts %}
        {% if post.featured == true %}
            {% include featuredbox.html %}
        {% endif %}
    {% endfor %}
    </div>
</section>
