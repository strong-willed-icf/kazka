1. [Альбом (PDF файл)](/downloads/svitkazky-design.pdf){: target="_blank" }
2. [План подвала](/assets/images/design/svitkazky-plan-basement.jpg){: .lightbox-image}
3. [План первого этажа](/assets/images/design/svitkazky-plan-floor-1.jpg){: .lightbox-image}
4. [План второго этажа](/assets/images/design/svitkazky-plan-floor-2.jpg){: .lightbox-image}
5. [План кровли](/assets/images/design/svitkazky-plan-roof.jpg){: .lightbox-image}
6. [Разрез 1 - 1](/assets/images/design/svitkazky-plan-cut-through-1-1.jpg){: .lightbox-image}
7. [Фасад 1 - 3](/assets/images/design/svitkazky-plan-facade-1-3.jpg){: .lightbox-image}
8. [Фасад 3 - 1](/assets/images/design/svitkazky-plan-facade-3-1.jpg){: .lightbox-image}
9. [Фасад А - Ж](/assets/images/design/svitkazky-plan-facade-А-Ж.jpg){: .lightbox-image}
10. [Фасад Ж - А](/assets/images/design/svitkazky-plan-facade-Ж-А.jpg){: .lightbox-image}