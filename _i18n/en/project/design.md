1. [Album (PDF)](/downloads/svitkazky-design.pdf){: target="_blank" }
2. [Basement plan](/assets/images/design/svitkazky-plan-basement.jpg){: .lightbox-image}
3. [First floor plan](/assets/images/design/svitkazky-plan-floor-1.jpg){: .lightbox-image}
4. [Second floor plan](/assets/images/design/svitkazky-plan-floor-2.jpg){: .lightbox-image}
5. [Roof plan](/assets/images/design/svitkazky-plan-roof.jpg){: .lightbox-image}
6. [Cut-through plan 1 - 1](/assets/images/design/svitkazky-plan-cut-through-1-1.jpg){: .lightbox-image}
7. [Facade 1 - 3](/assets/images/design/svitkazky-plan-facade-1-3.jpg){: .lightbox-image}
8. [Facade 3 - 1](/assets/images/design/svitkazky-plan-facade-3-1.jpg){: .lightbox-image}
9. [Facade А - Ж](/assets/images/design/svitkazky-plan-facade-А-Ж.jpg){: .lightbox-image}
10. [Facade Ж - А](/assets/images/design/svitkazky-plan-facade-Ж-А.jpg){: .lightbox-image}